This is the repository for a `Link Maker` code test.
Author: Simon Pollard

## How do I set up?

* __Clone the repository.__
* `cd` into the project folder.
 * Run `composer install` if you have composer installed on your machine.
 * Run `php composer.phar install` (you may need to sudo this) if you dont have composer.

Use the following command:

* `/usr/bin/php -q [full path to site]/public/convert.php key=[KEY] action=[ACTION] file=[FILE] store[STORE]` 
* **[key]** the random key found in convert.php
* **[action]** the required action to run, eg create-html
* **[file]** the file name - note file must be in the public/markdown folder
* **[store]** how to store the file - for this example `local` is the only option

This will generate the file in the public/html folder.

Note: all logs are generate in the logs folder in the root of the projects