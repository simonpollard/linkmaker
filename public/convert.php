<?php
/**
* Description: Converts markdown to HTML
* Info: called via command line, eg /usr/bin/php -q [full path to site]/public/convert.php key=[KEY] action=[ACTION]
* Author: Simon Pollard
* Version: 1.0.0
*/

// Set the default timezone for klogger to use
date_default_timezone_set('Europe/London');
// Require the vendor autoload so we can access the packages
require_once dirname(__FILE__) . '/../vendor/autoload.php';

// Get the paramaters and allow them to be accessed
global $args;
$args = $argv;

/**
 * Convert Class
 */
class Convert {

  //Whitelist your tasks - to stop people guessing
  private $whitelist = [
    'create-html',
  ];

  // Just a random string...
  private $key = 'yTra8c44Pf*K=8K';

  private $logger;

  public function __construct() {

    // Set logging
    $this->logger = new \Katzgrau\KLogger\Logger(__DIR__.'/../logs',
      \Psr\Log\LogLevel::DEBUG, array (
      'filename' => 'convert_'.date('Y-m').'.log'
    ));

    global $args;

    // Check for values
    if(empty($args)) {
      $this->logger->error('Missing arguments');
      return false;
    }
    if(!isset($args[1])) {
      $this->logger->error('Missing key parameter');
      return false;
    }
    if(!isset($args[2])) {
      $this->logger->error('Missing action parameter');
      return false;
    }
    if(!isset($args[3])) {
      $this->logger->error('Missing file parameter');
      return false;
    }
    if(!isset($args[4])) {
      $this->logger->error('Missing store parameter');
      return false;
    }

    // Set up our data
    $key    = str_replace('key=', '', $args[1]);
    $action = str_replace('action=', '', $args[2]);
    $file   = str_replace('file=', '', $args[3]);
    $store  = str_replace('store=', '', $args[4]);

    // Check we have a valid key
    if( $key != $this->key ) {
      $this->logger->error('Invalid key');
      return false;
    }

    // Check its a valid action
    if ( !in_array($action, $this->whitelist) ) {
      $this->logger->error('Action missing or not in whitelist: '.$_GET['action']);
      return false;
    }

    // Check we have been spcified a file
    if($file == '') {
      $this->logger->error('No file provided');
      return false;
    // if so check its the correct format
    } elseif (strpos($file, '.md') == false) {
      $this->logger->error('Invalid file format');
      return false;
    }

    // Functions should only run if an action is defined
    // This allows us to run other functions if we wanted a different action on the file etc
    switch ( $action ) {
      case 'create-html':
        $this->create_html($file, $store);
        break;

      default:
        break;
    }

  } // __construct

  /**
   * Create HTML file
   * @param  str $file The filename of the md file
   * @param  str $store Where to store the converted HTML
   */
  private function create_html($file, $store) {
    $Parsedown = new Parsedown();
    // Use parsedown in safe mode for processing untrusted user-input
    $Parsedown->setSafeMode(true);

    // Get the markdown text - this could be passed through as a paramater from the initial call, but tasks suggests we already know its location etc...
    $markdown_text = file_get_contents(__DIR__.'/markdown/'.$file);
    // If the file is not found
    if(!$markdown_text) {
      $this->logger->error('Unable to locate file');
      return false;
    }

    // Define the documment header
    $header = '<!DOCTYPE html>
    <html>
    <body>
    ';

    // Define the documment footer
    $footer = '
    </body>
    </html>';

    // Add the header
    $html = $header;
    // The content
    $html = $html.$Parsedown->text($markdown_text);
    // And the footer
    $html = $html.$footer;
  
    // Now check where we should store our file
    switch ($store) {
      
      // Save the file to a local folder
      case 'local':
        // Use the markdown file name
        $filename = __DIR__.'/html/'.str_replace('.md', '.html', $file);
        // Now create the file
        $handle = fopen($filename, 'w') or die($this->logger->error('Cannot open file: '.$my_file));
        // Write the html to the file
        fwrite($handle, $html);
        $this->logger->info($filename.' file created');
        break;

      // Potential for other storage locations
      case 'aws':
        break;

      case 'ftp':
        break;
      
      default:
        # code...
        break;
    }


  }

}

new Convert();